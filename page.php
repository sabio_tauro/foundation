<?php
/**
 * Page
 */
get_header(); ?>

<div class="row">
    <div class="large-8 medium-8 small-12 columns">
        <?php if(have_posts()):
            while (have_posts()): the_post(); ?>
                <article <?php post_class(); ?>>
                    <?php the_title('<h1 class="page_title">', '</h1>');

                    if (has_post_thumbnail()): ?>
                        <div title="<?php the_title_attribute(); ?>">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    <?php endif;

                    the_content(); ?>
                </article>
    	    <?php endwhile;
        endif; ?>
    </div>
    <div class="large-4 medium-4 small-12 columns sidebar">
        <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer(); ?>