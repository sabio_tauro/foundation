<?php
/*
 * Template Name: Home Page
 */
get_header();

home_slider_template();

if(have_posts()): ?>
	<div class="row">
	    <div class="small-12 columns">
            <?php while(have_posts()): the_post();
            	the_content();
            endwhile; ?>
	    </div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>