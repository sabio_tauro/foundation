<?php
/**
 * Header
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <!-- Set up Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header">
    <div class="row collapse medium-uncollapse">
        <div class="header-logo columns small-5 medium-2">
            <a href="<?php echo esc_url(home_url()); ?>">
                <img src="<?php echo(get_header_image()); ?>" alt="<?php echo(get_bloginfo('title')); ?>" />
            </a>
        </div>
        <div class="header-menu columns small-12 medium-10">
            <div class="title-bar" data-responsive-toggle="top-bar-menu" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle></button>
                <div class="title-bar-title"><?php _e('Menu', 'foundation'); ?></div>
            </div>
            <nav class="top-bar" id="top-bar-menu">
                <div class="top-bar-right">
                    <?php theme_header_links(); ?>
                </div>
            </nav>
            
            <?php if(have_rows('social_networks_options', 'option')) : ?>
                <div class="soc-networks">
                    <?php while(have_rows('social_networks_options', 'option')) : the_row(); ?>
                        <a class="soc-network-item" href="<?php echo get_sub_field('url_option'); ?>" target="_blank">
                            <i class="fa <?php echo get_sub_field('network_option'); ?>"></i>
                        </a>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</header>